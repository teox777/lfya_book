---
title: Presentación
type: docs
---

Este libro trata sobre la teoría de los Lenguajes Formales y Automátas. Esta
basado en el curso que doy en la [Facultad de
Ingeniería](https://www.ingenieria.unam.mx/) de la [Universidad
Nacional Autónoma de México](https://www.unam.mx/).


---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img
alt="Creative Commons Licence" style="border-width:0"
src="cc.png" /></a><br /><span
xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Lenguajes Formales y
Autómatas</span> por <a xmlns:cc="http://creativecommons.org/ns#"
href="https://ivanvladimir.gitlab.io/lfya_book/" property="cc:attributionName"
rel="cc:attributionURL">Ivan Vladimir Meza Ruiz</a> se licencia bajo <a
rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative
Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>
. Basada en la obra que se encuentra <a xmlns:dct="http://purl.org/dc/terms/"
href="https://gitlab.com/ivanvladimir/lfya_book"
rel="dct:source">https://gitlab.com/ivanvladimir/lfya_book</a>.
