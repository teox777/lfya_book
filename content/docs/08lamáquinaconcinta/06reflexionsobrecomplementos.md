---
weight: 86
title: "Reflexión sobre el complemento"
description: "Se reflexiona sobre el complemento de los lenguajes aceptados por
una máquina"
---


Si recordamos de nuestro conceptualización de [máquina
computacional]({{< ref "../02lamáquinasinmemoria/01abstración_cont/" >}}) tal
y como se como se conceptualizó el lenguaje de cadenas aceptadas tiene un
complemento que son las cadenas rechazadas. En particular por cada _w_ se puede
**decidir** a través del análisis de la cadena si esta se acepta o no, y por lo
tanto para todo el lenguaje aceptado y para todo el complemento de cadenas
rechazadas.

<center>
{{< figure src="../../02lamáquinasinmemoria/cajanegra2.png" title="Abstración de máquina computacional separa en dos el lenguaje de todas las cadenas" >}}
</center>

Si revisamos los casos por tipo de máquina:

* **Autómatas finitos** para las tres variantes de autómatas que vimos sabemos
    los lenguajes regulares son **decidibles** es decir para cualquier lenguaje
    regular un AF va a decir si sus cadenas pertenecen o no.
* **Automatas de pila** también sabemos que los lenguajes libres de contexto son
    decidibles, el AP va a determinar si las cadenas de estos lenguajes
    pertenecen o no. 

### ¿Por qué decidibilidad es importante?

El determinar que una máquina es decidible y por lo tanto el tipo de lenguaje
también es importante porque nos garantiza que no importa lo grande o complejo
que el cálculo que se hace sea, la máquina eventualmente nos dirá si la cadena
siendo analizada es aceptada o no, entonces si un análisis está tardando mucho
solo es cuestión de esperar a que acabe.

### El caso de ALF

Para los Autómatas Lineales con Frontera no hemos visto como funcionan, sin
embargo se puede observar que son similares a las MT con una cinta
limitada por los símbolos _<_ y _>_. Esto pudiera ser similar a nuestra experiencia con nuestras
computadoras que tienen un espacio limitado en RAM o disco duro. Sin embargo, la
definición es más general los límites de la cinta en ALF se definen en término
del tamaño de la entrada, generalmente proporcional. En algunos casos los
límites se encontrarán alrededor de _w_, y en otros podrá haber un espacio
mayor, esto es dependiendo de la configuración que se determine. Formalmente
podemos escribir

{{< katex display >}}
| \langle \ldots \rangle | = kw+2
{{< /katex >}}

Es decir la longitud en la cinta entre las marcas de límites es proporcional a
la cadena _w_ más dos posiciones por la marca.

### El problema con MT y ALF

El problema con la definición de MT es que para determinar que una cadena
pertenece al lenguaje la única opción que tenemos es que eventualmente se pase
por un estado final, esto es diferente al caso de Autómatas Finitos y Autómatas
de Pila, ya que al visitar la cadena de izquierda a derecha es suficiente
para determinar si la cadena se acepta o se rechaza (dependiendo al estado al
que se llegue), es decir es fácil determinar que son máquinas **decidibles**;
para MT no es tan sencillo porque internamente la MT podría estar haciendo
cálculos en la cinta y no necesariamente progresando para resolver la
pertenencia de ésta en el lenguaje, es decir esos cálculo nunca la llevarán a
pasar por un estado final.

La pregunta para MT y ALF, se transforma en si hay un mecanismo para determinar
para una cadena _w_ para la cual la máquina lleva mucho tiempo analizándola que
no pertenece al lenguaje ya que  nunca pasarán por un estado final y por lo tanto deberían ser rechazadas. Por supuesto, se antoja a revisar
el comportamiento de la máquina y basado en esta determinar si efectivamente
nunca pasará por un estado final. Esto se puede hacer por caso, pero de forma
general veremos que sólo es posible para máquinas tipo ALF, y MT no. 

En el caso de ALF podemos agregar un contador de configuraciones de la cinta,
por cada transición acumulamos el contador ¿De qué nos sirve esto? Como la cinta
está limitada, sabemos que sólo puede guardar un número de configuraciones
finitas. Si revasamos ese número y no hemos aceptado a la cadena, quiere decir
que no la aceptaremos porque si hubiera sido aceptada ya hubieramos visto un
estado final, pero como no lo vimos los siguiente análisis sólo podrán repetir
las configuraciones conducentes a un estado no final y por lo tanto nunca
aparecerá, y por lo tanto la cadena no forma parte del lenguaje. 

### Conclusión sobre el caso de ALF

Toda esta argumentación, nos permite establecer que los ALF son **decidibles**.
Es decir que para todas las cadenas del tipo de lenguaje que aceptan
eventualmente la máquinas nos dirá que la cadena pertenece, y para las que no
pertenecen al complemento. 

## El complemento de un lenguaje **decidible** es **decidile**

Es fácil ver que una máquina decidible produce un lenguaje decidible, y su
complemento también será decidible, solo tenemos que invertir la respuesta de la
máquina (que ya es decidible) y tenemos una máquina que acepta al complemento.

<center>
{{< figure src="../Decidible.svg" title="Complemento de máquinas decidibles son decidibles" >}}
</center>
