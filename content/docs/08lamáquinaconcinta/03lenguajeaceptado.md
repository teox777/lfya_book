---
weight: 83
title: "El lenguaje aceptado por una MT"
description: "Se presenta presenta el lenguaje aceptado por una MT"
---

El lenguaje aceptado por una MT es el siguiente

{{< katex display >}}
L_{MT}=\{ w \in \Sigma^* | q_0w \vdash^* \alpha q_f\beta, p\in A \}
{{< /katex >}}

Es decir, el lenguaje aceptado por una MT está compuesto por todas las cadenas
_w_ tal que partiendo de una descripción con el estado inicial {{< katex >}} q_0 {{< /katex >}} y de una posición
inicial de la cadena pasan por otra descripción instantánea dónde el estado es
uno final {{< katex >}} q_f {{< /katex >}}. 

### Diferencias con otras máquinas

La siguiente tabla resumen las situaciones en que se acepta o rechaza una cadena

| Máquina | Acepta | Rechaza |
|:--|:--|:--|
| AF | Termina de analizar la cadena y termina en un estado final | Termina de analizar la cadena y termina en un estado no final o en algún momento no encontró una transición |
| AP | Termina de analizar la cadena y termina en un estado final | Termina de analizar la cadena y termina en un estado no final o en algún momento no encontró una transición |
|ALF | ?? | ?? |
| MT | Pasa por un estado final | En algún momento no encontró una transición |

Como podemos ver en el caso de la MT no es tan claro como en las otras máquinas,
ya que la cadena la trabajamos directamente en el elemento de memoria (cinta),
entonces no tenemos control sobre si ya terminamos o no de procesar la cadena,
entonces la única pista para saber si se acepta la cadena es si eventualmente
pasamos por un estado aceptor (final).

#### Ejemplo de cadena rechazada en MT

El siguiente es un ejemplo de una cadena no aceptada, ya que no hubo una
transición esperada para nuestro ejemplo de MT visto [aquí]({{< ref
"01máquinadeturing.md#ejemplo-de-mt"  >}}). 
<center>
{{< figure src="../anbn_reject.gif" title="Análisis fallido para la cadena aaabb" >}}
</center>

