---
weight: 8
title: "La máquina con cinta"
bookCollapseSection: true
---

{{< section >}}

## Enlaces para explorar

* <i>[¿Qué es una máquina de Turing? Explicación simple | Gina
    Tost](https://www.youtube.com/watch?v=Es2NwtUwVc0)</i>. (2018, octubre 9). [Video] </br>
* <i>[¿Qué es una máquina de
    Turing?](https://www.youtube.com/watch?v=iaXLDz_UeYY)</i>. (2018, junio 10). [Video] </br>
* <i>[Alan Turing, la máquina enigma y la inteligencia artificial 🏳️‍🌈🤓🤖
    Bully Magnets Historia
    Documental](https://www.youtube.com/watch?v=v5rPVciWAvw)</i>. (2021, junio
    27). [Video] </br>
* <i>[Utilizando el simulador JFLAP para una Maquina de
    Turing](https://www.youtube.com/watch?v=2luP6q3EYkM)</i>. (2013, diciembre
    13) [Video] </br>
* <i>[Turing Machines - How Computer Science Was Created By
    Accident](https://www.youtube.com/watch?v=PLVCscCY4xI)</i>. (2020, febrero
    4). [Video] </br>
* <i>[Turing Machines Explained -
    Computerphile](https://www.youtube.com/watch?v=PLVCscCY4xI)</i>. (2014,
    agosto 29). [Video] </br>
* <i>[Turing machines explained
    visually](https://www.youtube.com/watch?v=-ZS_zFg4w5k)</i>. (2017, mayo 17).
    [Video] </br>
* <i>[A Turing Machine -
    Overview](https://www.youtube.com/watch?v=E3keLeMwfHY)</i>. (2010, marzo 7). [Video]
    </br>
* <i>[Alan Turing's 100th Birthday -
    Doodle/Google](https://www.google.com/doodles/alan-turings-100th-birthday).</i>
    (2012, junio 23). [Sitio web]</br>
* <i>[Máquina de Turing](https://es.wikipedia.org/wiki/Máquina_de_Turing)</i>.
    [Wikipedia]
* <i>[Turing machine](https://en.wikipedia.org/wiki/Máquina_de_Turing)</i>.
    [Wikipedia]
* <i>[Turing machine
    equivalents](https://en.wikipedia.org/wiki/Turing_machine_equivalents)</i>.
    [Wikipedia]


## Lecturas recomendadas

* Turing, A.M. (1936). "On Computable Numbers, with an Application to the
    Entscheidungsproblem". Proceedings of the London Mathematical Society. 2
    (published 1937). 42: 230–265. [http://doi.org/10.1112/plms/s2-42.1.230]. 
* Turing, A.M. (1938). "On Computable Numbers, with an Application to the
    Entscheidungsproblem: A correction". Proceedings of the London Mathematical
    Society. 2 (published 1937). 43 (6): 544–6. [http://doi.org/10.1112/plms/s2-43.6.544].
    Reprinted in many collections, e.g. in The Undecidable, pp. 115–154;
    available on the web in many places.
* O’Regan, G. (2012). Alan Turing. In: A Brief History of Computing. Springer,
    London.  
    [[BiDi/UNAM](https://link-springer-com.pbidi.unam.mx:2443/chapter/10.1007/978-1-4471-2359-0_14#citeas)],
    [[Liga](https://doi.org/10.1007/978-1-4471-2359-0_14)]
* B. Jack Copeland ed. (2004), The Essential Turing: Seminal Writings in
    Computing, Logic, Philosophy, Artificial Intelligence, and Artificial Life
    plus The Secrets of Enigma, Clarendon Press (Oxford University Press),
    Oxford UK.
    [[Liga](https://global.oup.com/academic/product/the-essential-turing-9780198250807?cc=mx&lang=en&)]
