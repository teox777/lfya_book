---
weight: 74
title: "Automata Lineal con Frontera"
description: "Se presenta el concepto de Autómata Lineal con Frontera"
---

La máquina que puede reconocer lenguajes dependiente del contexto se denomina
Autómata Lineal con Frontera (ALF); sin embargo, no se ahondará con ejemplo ya que se
optará con el Autómata de Doble Pila (ADP) para reconocer a estos lenguajes. Lo
anterior es por una razón didáctica, ya que el funcionamiento de los ALF están más cercanos a la MT que
se verá en las siguientes secciones; mientras que ADP resultará más poderoso que
un ALF pero su funcionamiento relaciona mejor con AP.


### ALF

<div class="definition">
Un <mark>Autómata de Lineal con Frontera (ALF)</mark> es una tupla {{< katex >}}(Q,\Sigma,\Gamma,q_0,B,A, \delta ){{< /katex >}} donde:

* {{< katex >}}Q{{< /katex >}} es un conjunto de estados finitos
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de símbolos terminales
* {{< katex >}}\Gamma{{< /katex >}} es un alfabeto de la cinta tal que {{< katex>}}\Sigma \subset \Gamma {{< /katex >}}  
* {{< katex >}}q_0{{< /katex >}} es un estado que denominaremos inicial donde
    {{< katex >}}q_0 \in Q{{< /katex >}}
* {{< katex >}}B{{< /katex >}} es un símbolo de espacio en blanco
* {{< katex >}}A{{< /katex >}} es un conjunto de estados que denominaremos
    finales donde {{< katex >}}A \subset Q{{< /katex >}}
* {{< katex >}}\delta{{< /katex >}} es una función de transición que cumple con:

{{< katex display>}}\delta:Q \times (\Gamma\cup \{<, >\}) \rightarrow Q \times
    (\Gamma\cup \{<, >\}) \times \{left,right\}  {{< /katex >}}
</div>

Donde _<_ y _>_ son marcas sobre la cinta que marcan el final de esta; y _left_
y _right_ son acciones de moverse una celda a la izquierda y a la derecha
respectivamente.

### Lo que sabemos

Con lo dicho para ALF esto es lo que sabemos:

| Lenguaje | Gramática | Máquina | Ejemplo  |
|:--------:|:---------:|:-------:|:--------:|
| ?? | ?? | Autómata de Doble Pila | ?? |
| Lenguaje Dependientes del Contexto | {{< katex >}} \gamma A \beta \rightarrow \gamma \alpha \beta {{< /katex >}} | Autómata Lineal con Frontera | {{< katex >}} a^nb^nc^n {{< /katex >}} |
| Lenguaje Independiente del Contexto | {{< katex >}} A \rightarrow \alpha {{< /katex >}} | Autómata de Pila | {{< katex >}} a^nb^n {{< /katex >}} |
| Lenguaje Regular | {{< katex >}} A \rightarrow bC | d {{< /katex >}} | AF, AFND y AFND-ε  | {{< katex >}} a^n {{< /katex >}} |


