---
weight: 71
title: "Gramáticas monotonicas"
description: "Se presenta el concepto de Gramática Monotónica"
---


<div class="definition">
Una <mark>Gramática Monotónica (GLDC)</mark> es una tupla {{< katex >}}(V,\Sigma,P,S){{< /katex >}} donde:

* {{< katex >}}V{{< /katex >}} es un alfabeto de <mark>símbolos no
    terminales</mark> o símbolos auxiliares
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de <mark>símbolos
    terminales</mark>, que conforman nuestras cadenas
* {{< katex >}}P{{< /katex >}} es un <mark>conjunto de reglas</mark> que con la forma
    {{< katex >}}\gamma \rightarrow  \alpha {{< /katex >}} donde 
    {{< katex >}}\alpha \in \left(\Sigma \cup V \right)^*, \gamma \in
    \left(\Sigma \cup V \right)^*{{< /katex >}} y
    {{< katex >}} |\gamma | \le |\alpha | {{< /katex >}} y
    {{< katex >}} S \rightarrow \epsilon {{< /katex >}}
* {{< katex >}}S \in V{{< /katex >}} lo denominamos <mark>símbolo inicial</mark>
</div>

Una GM es equivalente a una GDC, es decir ambos tipos de gramática generan el
mismo tipo de lenguajes. Los lenguajes dependientes del contexto.

### Ejemplo de GM

Un ejemplo de Gramática Monotónica es:

 {{< katex >}}\left(\{S\},\{a,b\},P,S \right){{< /katex >}} donde _P_:
 {{< katex display >}} 
 \begin{array}{ll}
 S \rightarrow &  aSBc \\ 
 S \rightarrow &  abc \\ 
 cB \rightarrow &  Bc \\ 
 bB \rightarrow &  bb \\ 
 \end{array}
 {{< /katex >}} 

### Derivación

A continuación
un ejemplo de derivación para la cadena {{< katex >}}a^nb^nc^n{{< /katex >}}.

{{< katex display >}} 
\begin{array}{rl}
\underline{S} \Rightarrow &  a\underline{S}Bc\\
\Rightarrow &  aa\underline{S}BcBc\\
\Rightarrow &  aaab\underline{cB}cBc\\
\Rightarrow &  aaabBc\underline{cB}c\\
\Rightarrow &  aaabB\underline{cB}cc\\
\Rightarrow &  aaa\underline{bB}Bccc\\
\Rightarrow &  aaab\underline{bB}ccc\\
\Rightarrow &  aaabbbccc\\
\end{array}
{{< /katex >}}

Como se puede apreciar las GM son más compactas que las GDC, ya que no tienen
que respetar las restricciones del contexto.
