---
weight: 7
title: "Revisando la jerarquía de Chomsky"
bookCollapseSection: true
---

{{< section >}}


## Enlaces para explorar

* <i>[Noncontracting grammar](https://en.wikipedia.org/wiki/Noncontracting_grammar)</i>. (s/f) [Wikipedia]
* <i>[Forma Normal de Chomsky](https://es.wikipedia.org/wiki/Forma_normal_de_Chomsky)</i>. (s/f) [Wikipedia]
* <i>[Forma Normal de Greibach](https://es.wikipedia.org/wiki/Forma_normal_de_Greibach)</i>. (s/f) [Wikipedia]
* <i>[Chomsky Normal Form](https://en.wikipedia.org/wiki/Chomsky_normal_form)</i>. (s/f) [Wikipedia]
* <i>[Greibach Normal Form](https://en.wikipedia.org/wiki/Greibach_normal_form)</i>. (s/f) [Wikipedia]
* <i>[Kuroda Normal Form](https://en.wikipedia.org/wiki/Kuroda_normal_form)</i>. (s/f) [Wikipedia]
* <i>[Autómata Lineal con Frontera](https://es.wikipedia.org/wiki/Aut%C3%B3mata_linealmente_acotado)</i>. (s/f) [Wikipedia]
* <i>[Linear Bounded Automaton](https://en.wikipedia.org/wiki/Linear_bounded_automaton)</i>. (s/f) [Wikipedia]
* <i>[Linear Bounded Automata (LBA) Definition](https://www.youtube.com/watch?v=8sMIKw8epT8)</i>. (2021, ene 19). [Video]</br>
* <i>[Chomsky Hierarchy](https://en.wikipedia.org/wiki/Chomsky_hierarchy)</i>. (s/f) [Wikipedia]
* <i>[Jerarquía de Chomsky](https://es.wikipedia.org/wiki/Jerarqu%C3%ADa_de_Chomsky)</i>. (s/f) [Wikipedia]

## Lecturas recomendadas
* Chomsky, N. (1956). Three models for the description of language. IRE
    Transactions on Information Theory, 2(3), 113–124.
    [[Liga]](https://doi.org/10.1109/TIT.1956.1056813)
* Chomsky, N. (1963). "Formal properties of grammar". In Luce, R. D.; Bush, R.
    R.; Galanter, E. (eds.). Handbook of Mathematical Psychology. New York:
    Wiley. pp. 360–363.
    [[Liga]](https://archive.org/details/handbookofmathem017893mbp)
* Greibach, S. A. (1965). A New Normal-Form Theorem for Context-Free Phrase
    Structure Grammars. J. ACM, 12(1), 42–52. 
    [[Liga]](https://doi.org/10.1145/321250.321254)
* Blum, N., & Koch, R. (1997). Greibach Normal Form Transformation, Revisited.
    Information and Computation, 150, 47–54.
    [[Liga]](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.47.460)
    [[BiDi/UNAM]](https://doi-org.pbidi.unam.mx:2443/10.1006/inco.1998.2772)
* Kuroda, S. (1964). "Classes of languages and linear-bounded
    automata". Information and Control. 7 (2): 207–223.
    [[Liga]](https://doi.org/10.1016%2FS0019-9958%2864%2990120-2)
    [[BiDi/UNAM]](https://doi-org.pbidi.unam.mx:2443/10.1016/S0019-9958%2864%2990120-2)
* Holt, D. F., Rees, S., & Röver, C. E. (2017). Groups, languages and
    automata. Cambridge University Press. **Capítulo 12**
    [[BiDi/UNAM]](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001959658&lang=es&site=eds-live)
    [[Liga]](https://www.cambridge.org/core/books/groups-languages-and-automata/D31C153F123E5364D1690F697A0F16F2)]
