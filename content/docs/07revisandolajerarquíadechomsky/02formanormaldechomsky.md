---
weight: 72
title: "Forma Normal de Chomsky"
description: "Se presenta la Forma Normal de Chomsky"
---

En alguna ocasiones es posible transformar una Gramática en un otra debilmente
equivalente, es decir que genera exactamente el mismo lenguaje pero con una
estructura de árbol de derivación diferente. Alguna veces es algo conveniente
porque los árboles de derivación de la nueva gramática podrían tener algunas
propiedades de las cuales se podría tomar ventaja.


### FNC

Toda GLC se puede transformar en una gramática en la Forma Normal de Chomsky que
requiere que las reglas tomen la siguiente forma:

 {{< katex display >}} 
 \begin{array}{ll}
 A \rightarrow &  BC \\ 
 A \rightarrow &  a \\ 
 A \rightarrow &  \varepsilon \\ 
 \end{array}
 {{< /katex >}} 

### Proceso de transformación

Para transformar una gramática {{< katex >}}(V,\Sigma,P,S){{< /katex >}} en su
FNC se siguen los siguientes pasos:

0. Agregar un nuevo símbolo inicial
 {{< katex display >}} 
 \begin{array}{ll}
 S_0 \rightarrow &  S \\ 
 \end{array}
 {{< /katex >}} 

1. Quitar las transiciones _ε_
   * Identificar las producción {{< katex >}} A \rightarrow \varepsilon {{< /katex >}}
   * Producir nuevas producciones suponiendo que dónde aparece _A_ es _ε_; por
       ejemplo si {{< katex >}} P \rightarrow AxB; A \rightarrow \varepsilon; B \rightarrow \varepsilon{{< /katex >}} entonces se producen las nuevas
       reglas para _P_:
 {{< katex display >}} 
 \begin{array}{ll}
 P \rightarrow &  xB | Ax | x \\ 
 \end{array}
 {{< /katex >}}
    * Eliminar de la gramática las transiciones _ε_

2. Quitar las transiciones unitarias
   * Identificar las producción {{< katex >}} A \rightarrow B {{< /katex >}}
   * Por cada {{< katex >}} B \rightarrow \alpha {{< /katex >}} agregar las
       reglas:
 {{< katex display >}} 
 \begin{array}{ll}
 A \rightarrow &  \alpha \\ 
 \end{array}
 {{< /katex >}}
    * Eliminar de la gramática las unitarias

3. Quitar las transiciones largas
   * Identificar las producción {{< katex >}} A \rightarrow A_0A_1A_2\ldots A_m {{< /katex >}}
   * Cortarlas usando símbolos auxiliares para encadenar la regla
 {{< katex display >}} 
 \begin{array}{rl}
 A \rightarrow &  A_0T_1 \\ 
 T_1 \rightarrow &  A_1T_2 \\ 
 T_2 \rightarrow &  A_2T_3 \\
 \vdots \rightarrow & \vdots \\
 T_{m-1}\rightarrow &  A_{m-1}A_{m} \\ 
 \end{array}
 {{< /katex >}}

 4. Quitar los terminales binarios 
    * Identificar las producción {{< katex >}} A \rightarrow aB {{< /katex >}}
    * Agregar una regla con el no terminal (si es necesario) y remplazar en la
       regla original la aparición del terminal con ese no terminal
{{< katex display >}} 
\begin{array}{rl}
A \rightarrow &  N_aB \\ 
N_a \rightarrow &  a \\ 
\end{array}
{{< /katex >}}


### Ejemplo 

Reducir la siguiente gramática en su Forma Normal de Chomsky:
{{< katex display >}} 
\begin{array}{rl}
S \rightarrow &  aSA | BSB | D\\ 
A \rightarrow &  C  \\ 
C \rightarrow &  a  \\ 
B \rightarrow &  b  \\ 
D \rightarrow &  \varepsilon \\ 
\end{array}
{{< /katex >}}


0. Agregar un nuevo símbolo inicial
    {{< katex display >}} 
    \begin{array}{lll}
    R_0 \rightarrow &  S & \text{Nuevo símbolo}\\ 
    S \rightarrow &  aSA | BSB | D &\\ 
    A \rightarrow &  C  &\\ 
    C \rightarrow &  a  &\\ 
    B \rightarrow &  b  &\\ 
    D \rightarrow &  \varepsilon & \\ 
    \end{array}
    {{< /katex >}} 

1. Quitar las transiciones _ε_
 {{< katex display >}} 
 \begin{array}{lll}
 R_0 \rightarrow &  S | \varepsilon & \text{Se agregó por } S \rightarrow \varepsilon \\ 
 S \rightarrow &  aSA | BSB | aA | BB & \text{Se agrego por } B \rightarrow \varepsilon \\ 
 A \rightarrow &  C  &\\ 
 C \rightarrow &  a  &\\ 
 B \rightarrow &  b  &\\ 
 \end{array}
 {{< /katex >}}

2. Quitar las transiciones unitarias
 {{< katex display >}} 
 \begin{array}{lll}
 R_0 \rightarrow &  S & \\ 
 S \rightarrow &  aSA | BSB | aA | BB &  \\ 
 A \rightarrow &  a  & \text{Se agrego por unitaria}\\ 
 C \rightarrow &  a  &\\ 
 B \rightarrow &  b  &\\ 
 \end{array}
 {{< /katex >}}

3. Quitar las transiciones largas
 {{< katex display >}} 
 \begin{array}{lll}
 R_0 \rightarrow &  S  & \\ 
 S \rightarrow &  aT_1 | BV_1 | aA | BB & \text{Se redujeron por regla larga} \\ 
 T_1 \rightarrow &  SA  &  \text{Se creo por regla larga}\\ 
 V_1 \rightarrow &  SB  &  \text{Se creo por regla larga}\\ 
 A \rightarrow &  a  & \\ 
 C \rightarrow &  a  &\\ 
 B \rightarrow &  b  &\\ 
 \end{array}
 {{< /katex >}}

4. Quitar los terminales binarios 
 {{< katex display >}} 
  \begin{array}{lll}
 R_0 \rightarrow &  S  & \\ 
 S \rightarrow &  N_aT_1 | BV_1 | N_aA | BB & \text{Se modifico por terminales binarios} \\ 
 T_1 \rightarrow &  SA  &\\ 
 V_1 \rightarrow &  SB  &\\ 
 N_a \rightarrow &  a & \text{Se creo por terminales binarios} \\ 
 A \rightarrow &  a  & \\ 
 C \rightarrow &  a  &\\ 
 B \rightarrow &  b  &\\ 
 \end{array}
 {{< /katex >}}

### Comparación de árboles de derivación

La gramática original produce el siguiente árbol para la cadena _abaaba_

<center>
{{< figure src="../original.png" title="Arbol de derivacion para abaaba con gramatica original" >}}
</center>

Mientras que la gramática resultante en FNC produce

<center>
{{< figure src="../fnc.png" title="Arbol de derivacion para abaaba con gramática en FNC" >}}
</center>

Como se puede observar las dos cadenas son aceptadas, pero producen diferentes
árboles de derivación. Una propiedad importante de la FNC es que cada rama del
árbol sólo tiene dos hijos posibles. 
