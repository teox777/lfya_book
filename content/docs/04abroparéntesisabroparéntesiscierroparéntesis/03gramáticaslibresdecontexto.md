---
weight: 43
title: "Gramaticas libres de contexto"
description: "Se presentan las Gramáticas Libres de Contexto"
---

En la sección anterior identificamos al lenguaje formado por cadenas del tipo
{{< katex >}} a^nb^n {{< /katex >}} que no es regular _¿Entonces qué es?_ Antes
de poder explicar más sobre este lenguaje vamos a presentar el concepto de
gramáticas a través del tipo denominado _libres de contexto_, que nos ayudarán a
generar cadenas de este lenguaje.

### GLC

<div class="definition">
Una <mark>Gramática Libre de Contexto (GLC)</mark> es una tupla {{< katex >}}(V,\Sigma,P,S){{< /katex >}} donde:


* {{< katex >}}V{{< /katex >}} es un alfabeto de <mark>símbolos no
    terminales</mark> o símbolos auxiliares
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de <mark>símbolos
    terminales</mark> que conforman nuestras cadenas
* {{< katex >}}P{{< /katex >}} es un <mark>conjunto de reglas</mark> con la forma
    {{< katex >}}A \rightarrow  \alpha{{< /katex >}} donde 
    {{< katex >}}\alpha \in \left(\Sigma \cup V \right)^*{{< /katex >}}  
* {{< katex >}}S \in V{{< /katex >}} lo denominamos <mark>símbolo inicial</mark>
</div>


{{< hint info >}}
**Algunas preferencias en la notación**
* Es usual definir los símbolos no terminales con mayúsculas.
* Es usual definir los símbolos terminales con minúsculas.
* Es usual usar _S_, de _start_, o _R_, de _root_, como símbolos iniciales.
* En las reglas, a la parte izquierda de estas (antes de la flecha) se le
    denomina _cabeza_ de la regla, y la parte derecha (después de la flecha) se
    le denomina _cuerpo_ de la regla.
{{< /hint >}}

#### Ejemplo de GLC

Un ejemplo de gramática es  {{< katex >}}\left(\{S\},\{a,b\},\{S\rightarrow aSb,S\rightarrow \varepsilon\},S \right){{< /katex >}} 

También es correcto definirla de la siguiente forma:


 {{< katex >}}\left(\{S\},\{a,b\},P,S \right){{< /katex >}} donde _P_:
 {{< katex display >}} 
 \begin{array}{ll}
 S \rightarrow & aSb\\ 
 S \rightarrow & \varepsilon 
 \end{array}
 {{< /katex >}} 

<blockquote class="book-hint info">
  <p><strong>Algunas preferencias en la notación</strong></p>

* Es común usar una <em>|</em> para agrupar reglas que tienen la misma cabeza. De tal
    forma que la definición puede ser:

 {{< katex >}}\left(\{S\},\{a,b\},P,S \right){{< /katex >}} donde _P_:
 {{< katex display >}} 
 \begin{array}{ll}
 S \rightarrow & aSb | \varepsilon 
 \end{array}
 {{< /katex >}} 

* También es común omitir la tupla y sólo poner las reglas; en este caso los
    elementos se infieren de las reglas. De tal forma que la definición de la
    gramática puede ser simplemente:

{{< katex display >}} 
 \begin{array}{ll}
 S \rightarrow & aSb | \varepsilon 
 \end{array}
 {{< /katex >}} 
 </blockquote>


### Proceso de re-escritura

Las gramáticas se interpretan con un proceso de re-escritura; uno comienza del
símbolo inicial y re-escribe símbolos no terminales hasta que lo que quede sea
una cadena conformada por símbolos no terminales. 


Al camino seguido de re-escritura para generar una cadena se
denomina <mark>derivacion</mark>; para señalar los pasos intermedios se usa el
símbolo {{< katex >}}\Rightarrow{{< /katex >}}.

### Ejemplo de derivación

Usando la gramática {{< katex >}} S \rightarrow aSb | \varepsilon  {{< /katex >}} 
se puede generar la siguiente derivación:

{{< katex display >}} 
\begin{array}{rll}
\underline{S} &\Rightarrow & a\underline{S}b \\
              &  \Rightarrow & aa\underline{S}bb \\
              &  \Rightarrow & aaa\underline{S}bbb \\
              &  \Rightarrow & aaaa\underline{S}bbbb \\
              &  \Rightarrow & aaaaa\underline{S}bbbbb \\
              & \Rightarrow & aaaaa\varepsilon bbbbb = aaaaabbbbb\\
\end{array}
{{< /katex >}} 

<blockquote class="book-hint info">
  <p><strong>Algunas preferencias en la notación</strong></p>

* Es común usar el símbolo {{< katex >}} \Rightarrow^* {{< /katex >}} para
    significar que el siguiente paso en realidad requiere múltiples pasos
    intermedios. Por ejemplo, la cadena anteriormente generada puede escribirse
    como:
    {{< katex display >}} 
    \begin{array}{rll}
    \underline{S} &\Rightarrow^* &  aaaaabbbbb\\
    \end{array}
    {{< /katex >}} 

</blockquote>


### El lenguaje generado por una GLC

Debe ser intuitivo que la gramática de ejemplo hasta este momento genera únicamente cadenas de la forma 
{{< katex >}} a^nb^n {{< /katex >}}. En este momento tenemos un gramática que
genera el lenguaje que no es regular. Como existen muchas gramáticas, hay muchos
lenguajes generados por las GLC, y muchos de estos lenguajes no son Lenguajes
Regulares. A los lenguajes generados por las GLC se les denomina **<mark>Lenguajes
Libres de Contexto</mark>**. 

Formalmente un lenguje _L_ generado por una
gramática _G_  se define como {{< katex >}} L=\{ w | S \Rightarrow^* w \} {{< /katex >}}
donde _S_ es el símbolo inicial de la gramática {{< katex >}}G=\left(V,\Sigma,P,S \right){{< /katex >}}. Es decir el lenguaje
 de asociado a una gramática es el conjunto de todas las cadenas que se 
 derivan partiendo del símbolo inicial y siguiendo las reglas de producción.

