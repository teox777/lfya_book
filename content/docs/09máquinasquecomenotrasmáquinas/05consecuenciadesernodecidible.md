---
weight: 95
title: "Consecuencia de ser no decidible"
description: "Se presentan varias observaciones sobre las consecuencias de que
problema del paro sea no decidible"
---

Sabemos que _M<sub>h</sub>_ es no decidible, porque de serlo _Z_ tendría que
existir y no podríamos saber si se acepta o no. _M<sub>h</sub>_ es nuestra
primera máquina que no es decidible, es decir que no puede determinar para todas
sus entradas si las acepta o rechaza. Si este es el caso, entonces que hace una
que intente resolver el problema, lo que pasará es que además de la opción de
parar existe la opción de que se cicle, es decir que para cierta entrada nunca
de respuesta. 

Pero por otro lado sabemos que existen MT que dada cualquier entrada van a
parar, es decir que son decidibles. Esto significa que habrá dos tipos de
lenguajes aceptados por las MT, los decidibles y los no decidibles.

### Revisando la MTU 

Si una MTU simula a una MT, que pasa si la MT que revisa es no decidible,
entonces la ejecución de la MT será no decidible. Como el comportamiento de la
MTU depende de la máquina que simula, la MTU es no decidible también.

### Dentro de malas noticias un poco de buenas noticias

_M<sub>h</sub>_ no es imposible de construir, podemos usar una MTU para
simularla, lo único que tenemos que hacer pasar la entrada de _M<sub>h</sub>_
una _M<sub>U</sub>_ y esperar si para. Por supuesto, esta estrategia hace al
problema de paro no decidible, es decir para algunas MT se podrá decir y para
otras no, ya que están cicladas.

Por otro lado hay que notar que dado el funcionamiento de MT si la cadena de
entrada se va aceptar, eventualmente la MT parará no importa lo complicado del
cómputo, eventualmente parará. Sin embargo si la entrada se debe rechazar, es
cuando el caso de ciclado se puede dar. El problema práctico que para una
entrada _w_ muchas veces no sabemos si debe ser aceptada o rechazada, y además
no sabemos diferenciar si mientras está siendo procesada por nuestra MT se trata
de un caso de que requiere más tiempo para procesar o simplemente ya se cicló.
Por supuesto se antoja construir un mecanismo que decida durante ejecución si la
máquina ya está ciclada, pero es exactamente lo que demostramos ese mecanismo
también se podría quedar ciclado haciendo todo el proceso no decidible.

### Lenguajes Recursivos

Los <mark>Lenguajes Recusivos (R)</mark> son aquellos asociados a una MT decidible.

### Lenguajes Recursivamente Enumerables

Los <mark>Lenguajes Recursivamente Enumerables (RE)</mark> son aquellos para los cuales se puede
construir una MT que podrá verificar una cadena aceptada, pero no podrá
verificar una cadena rechazada ya que podrá ciclarse. Cabe aclarar que los
Lenguajes Recusivamente Eneumerables contienen a los recursivos. 

### La Jerarquía de Chomsky

| Lenguaje | Gramática | Máquina | Ejemplo  |
|:--------:|:---------:|:-------:|:--------:|
| RE/R |  {{< katex >}} \alpha \rightarrow \beta {{< /katex >}} | MT,ADP,MTₖ,... | A<sub>MT</sub>, L<sub>h</sub>/?? |
| Lenguaje Dependientes del Contexto | {{< katex >}} \gamma A \beta \rightarrow \gamma \alpha \beta {{< /katex >}} | Autómata Lineal con Frontera | {{< katex >}} a^nb^nc^n {{< /katex >}} |
| Lenguaje Independiente del Contexto | {{< katex >}} A \rightarrow \alpha {{< /katex >}} | Autómata de Pila | {{< katex >}} a^nb^n {{< /katex >}} |
| Lenguaje Regular | {{< katex >}} A \rightarrow bC | d {{< /katex >}} | AF, AFND y AFND-ε  | {{< katex >}} a^n {{< /katex >}} |









