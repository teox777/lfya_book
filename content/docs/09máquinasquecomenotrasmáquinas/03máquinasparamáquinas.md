---
weight: 92
title: "Máquinas para máquinas"
description: "Se exploran escenarios donde MT reciben descripciones éstandar de MT"
---

### Problemas para las MT con MTs

La codificación de funciones de MT hace posible que una MT reciba como parte de
su entrada otra MT _¿Por qué querríamos eso?_

* Simular una _M_
* Procesan la codificación de una _M_
* Analizan el comportamiento de la máquina codificada _M_

El el caso de la simulación sabemos que se puede, la MTU no es tan sencilla de
construir pero es posible. Por otro lado para procesar aspectos de la
codificación de la MT hay cosas interesantes que se pueden hacer:

* Verificar el formato de la cadena
* Contar el número de estados
* Verificar si la cadena tiene un loop hacia un mismo estado de longitud de un
    símbolo
* Étcetera

Para el caso del comportamiento vamos a esperar un poco para poder sacar
conclusiones. 

### El truco de presentarse a si misma

Dado como hemos definido las MT que aceptan otras MT, nada evita que una MT _M_
que recibe en su entrada una codificación de una MT que esa entrada sea la
codificación de si misma; esta situación va a levantar aspectos interesantes. 

#### Para máquinas universales

Nada evita que en la MTU la máquina a simular sea la misma MTU. La entrada _w_ en este caso
tendría dos partes, una que sería una MT _M_ y una cadena que se probará en esa
máquina.

<center>
{{< figure src="../mtumtu.svg" title="Máquina de Turing Universali recibiendo como entrada a si misma" >}}
</center>

Este escenario se parece a la situación cuando una máquina virtual en un sistema
operativo, corre el mismo sistema operativo. El escenario es factible y nada
previene que no sea posible. Al contrario, la teoría predice que es posible
hacer esa simulación.

#### Para máquinas que procesan la codificación

El mismo caso se puede hacer para aquellas máquinas que identifican propiedades
basadas en las cadenas, por ejemplo: si una máquina _M<sub>#q</sub>_ cuenta el número de estados
presentes en la máquina que recibe como entrada, entonce si alimento a esa
máquina con su propia codificación __<M<sub>#q</sub>>__ lo que arrojará la
máquina es el número de estados que utiliza la máquina. Como vemos este
escenario es totalmente factible y hace sentido, de nuevo la teoría predice que
es posible.

### El lenguaje de las máquinas que se aceptan a si mismas

Usando _M<sup>U</sup>_ es posible construir una máquina M<sup>mm</sup>_que acepte aquellas
descripciones de máquinas que tienen la capacidad de aceptarse a si mismas. Como
esta existe esta MT, quiere decir que existe un lenguaje asociado
_L<sup>mm</sup>_. Este lenguaje está conformado por codificaciones de máquinas
que se aceptan a si mismas. Este lenguaje se refiere a máquinas que cumplen con
cierto comportamiento. 

<center>
{{< figure src="../mm.svg" title="Máquina de Turing para analisar si una MT se acepta a si misma" >}}
</center>

Si nos damos cuenta la _M<sub>mm</sub>_ usa la _M<sub>U</sub>_ para identificar
aquellas máquinas que se acepan a si mismas, al copiar la descripción tanto como maquina a
simular como entrada a esa máquina a simular.



