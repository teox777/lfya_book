---
weight: 91
title: "Codificación de una máquina de Turing"
description: "Se presenta un mecanismo para codificar una máquina de Turing"
---

Hasta ahora hemos visto como crear máquinas de Turing siguiendo los lineamientos
de la definición propuesta [aquí]({{<ref "../08lamáquinaconcinta/01máquinadeturing/">}}), 
es decir definimos los elementos de la tupla y establecemos la función de
transición, también hemos visto que ese objeto abstracto lo podemos representar
de forma gráfica o en forma de una tabla siguiendo las convenciones que hemos
establecido. Sin embargo, no son las únicas formas de representar a una MT, en
particular vamos a mostrar que la función de transición de una máquina de Turing
también es posible representarla como una cadena del alfabeto {{< katex
>}}\Sigma = \{0,1\} {{< /katex >}}.

### Elementos como números enteros

Lo primero que tenemos que hacer es asignar un entero a cada elemento de la
Máquina de Turing. Tenemos tres elementos: los estados _Q_, los símbolos de {{<katex >}}\Gamma {{< /katex >}}
y la dirección en la que se mueve la cinta. 

{{< katex display >}}
\begin{array}{ll}
M_Q(q) & \rightarrow  \N^+\\ 
M_{\Gamma}(a) & \rightarrow  \N^+\\
M_{\{left, right\}}(d) & \rightarrow  \N^+
\end{array}
{{< /katex >}}

Estos tres mapeos tienen que ser inyectivos, es decir para cada elementos de
dominio se le asignará sólo un número único (codiminio), el número a asignar es
a partir de uno. Por ejemplo, los
siguientes son mapeos posibles para la
máquina [ejemplo]({{<ref "../08lamáquinaconcinta/01máquinadeturing#ejemplo-de-mt">}}) 

{{< katex display >}}
\begin{array}{rl}
M_Q(q) &= \{q_0:1, q_1:2, q_2:3, q_3:4, q_4:5 \}\\ 
M_{\Gamma}(a) &= \{\mathfrak{B}:1, a:2, b:3,  X:4, Y:5\}\\
M_{\{left, right\}}(d) &= \{left:1, right:2\}
\end{array}
{{< /katex >}}

### Codificación de transiciones

Con esto en mente es posible codificar una transición de MT: 
{{< katex display >}}
\delta(q_i,X_j)= (q_k,X_l,D_m)
{{< /katex >}}
siguiendo la siguiente espeficiación:
{{< katex display >}}
0^{M_q(q_i)}10^{M_{\Gamma}(X_j)}10^{M_q(q_k)}10^{M_{\Gamma}(X_l)}10^{M_{\{left,right\}}(D_m)}
{{< /katex >}}

Es decir los elementos de la transición se codifican en una cadena donde cada
elemento está dividido por un uno y la cantidad de ceros indica el indice dentro
del mapeo correspondiente al elemento original. Si los indices de los elementos
de los alfabetos y la dirección los asociamos directamente a un mapeo podemos escribir la
codificación como:

{{< katex display >}}
0^i10^j10^k10^l0^m
{{< /katex >}}

Por ejemplo, para la transición {{< katex >}}\delta(q_0,a)=(q_1,X,R){{< /katex >}} 
 de la máquina [ejemplo]({{<ref "../08lamáquinaconcinta/01máquinadeturing#ejemplo-de-mt">}}) 
y el mapeo presentado [anteriormente](#elementos-como-números-enteros) su codificación es:

{{< katex display >}}
010010010000100
{{< /katex >}}


### Codificación de la función de transición

Ya que podemos codificar una transición, podemos codificar como una cadena toda
la función de transición, cada transición se separa por dos 11, de tal forma que
la tabla de transición para la máquina [ejemplo]({{<ref "../08lamáquinaconcinta/01máquinadeturing#ejemplo-de-mt">}}) 
y el mapeo presentado [anteriormente](#elementos-como-números-enteros):


{{< katex display >}}
\tiny{
01001001000010011
00101001010011
0010000010010000010011
0010001000001011
000101000101011
00010000010001000001011
00010000101000010011
01000001000010000010011
00001000001000010000010011
0000101000001010
}
{{< /katex >}}

A la codificiación resultante se le conoce como _descripción estándar_ de una
MT.

### Reflexión

Debe ser intuitivo que la codificación anterior se puede hacer para cualquier
MT. Con esto en mente hemos podido crear una cadena que representa el
comportamiento de dicha MT. Como cadena, nada evita que la pongamos en la cita
de otra MT.
