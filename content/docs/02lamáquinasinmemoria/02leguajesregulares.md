---
weight: 22
title: "Lenguajes regulares"
description: "Se definen a los lenguajes Regulares"
---

Vamos a definir a un tipo de lenguajes que denominaremos como <mark>Lenguaje Regulares</mark>
(<em>L<sub>reg</sub></em>). Para lograr esto se va usar una definición recursiva.

### Lenguaje Regular

Hay dos tipos de lenguajes regulares:

* Lenguajes <mark>regulares básicos</mark>
* y los resultantes de operaciones sobre lenguajes regulares.

#### Lenguajes regulares básicos

Un lenguaje {{< katex >}}L{{< /katex >}} es **regular** si se trata de uno
de los siguientes casos:

1. El lenguaje {{< katex >}}\emptyset{{< /katex >}}, es decir, el <mark>lenguaje
   vacío</mark>.
1. El lenguaje {{< katex >}}\{ \varepsilon \}{{< /katex >}}, es decir, el
   <mark>lenguaje de la cadena vacía</mark>.
1. El lenguaje {{< katex >}}\{a\}{{< /katex >}} donde {{<
   katex >}} a\in \Sigma {{< /katex >}}. A este lenguaje se le conoce como el
   <mark>lenguaje de un símbolo del alfabeto</mark>. 


#### Lenguajes regulares resultantes de operaciones 

Adicionalmente un lenguaje es **regular** si resulta de una de estas operaciones:

1. _Unión_

   Si {{< katex >}}L_1{{< /katex >}} y {{< katex >}}L_2{{< /katex >}} son
   regulares, entonces {{< katex >}}L_1 \cup L_2{{< /katex >}} es regular
   también.

1. _Concatenación_

   Si {{< katex >}}L_1{{< /katex >}} y {{< katex >}}L_2{{< /katex >}} 
   son regulares, entonces {{< katex >}} L_1 L_2{{< /katex >}} es regular
   también.

1. _Cerradura estrella_

   Si {{< katex >}}L{{< /katex >}} es
   regular, entonces {{< katex >}}L^*{{< /katex >}} es regular también.

1. _Priorización por paréntesis_

   Si{{< katex >}}L{{< /katex >}} es
   regular, entonces {{< katex >}}(L){{< /katex >}} es regular también.

---

En resumen un lenguaje {{< katex >}}L{{< /katex >}} es regular si corresponde
con uno de los casos básicos: _vacío_, _de la cadena vacía_ o _lenguaje de un
símbolo_ ; o se puede generar a través de una secuencia finita de
operaciones sobre lenguajes regulares a través de las operaciones de
_unión_, _concatenación_, _cerradura estrella_ y _priorización por paréntesis_.

### Ejemplo: número de _bes_ pares

¿Cómo lucen cadenas de este lenguaje?

{{< katex >}}\{ bb, bab, babaaaaaaa, aabaaaaabaaaabb, bbbbbbaaaaaaaabaaaaab,
bababaabaabab, bbbbbbbb, \ldots \}{{< /katex >}}

¿Cómo lucen cadenas que no están en este lenguaje?

{{< katex >}}\{ b, bbb, ab, ba, bbbbbaaaaaaaaaaaabb, \ldots \}{{< /katex >}}

¿Cómo podríamos generar este lenguaje a través de nuestras operaciones?

Partiendo de {{< katex >}}\{b\}{{< /katex >}} como regular, por ser uno de los
casos básicos:

1. {{< katex >}}\{b\}\{b\}{{< /katex >}} es regular por concatenación, dos _bes_
   seguidas
2. {{< katex >}}\{b\}\{a\}\{b\} {{< /katex >}}es regular por concatenación,
   puede aparecer una _a_ entre las dos _bes_
3. {{< katex >}}\{b\}\{a\}^∗\{b\} {{< /katex >}} es regular por cerradura,
   pueden aparecer cualquier cantidas de _aes_ entre las dos _bes_
4. {{< katex >}}\{a\}^∗\{b\}\{a\}^∗\{b\}\{a\}^∗ {{< /katex >}} es regular por
   concatenación, en realidad pueden aparecer cualquier cantidad de _aes_ antes, en medio
   y después de las _bes_
5. {{< katex >}}(\{a\}^∗\{b\}\{a\}^∗\{b\}\{a\}^∗)^∗ {{< /katex >}} pueden
   aparecer cualquier cantidad de veces el patrón de dos _bes_ con cualquier
   cantidad de _aes_ antes, en medio o después.

{{< hint danger >}}
**Construcción erronea**


La construcción anterior cubre la mayoría de los aspectos sin embargo deja fuera
el caso en que la cadena esté conformada por puras _aes_; dado que una vez en el
patrón de repetición que forza la cerradura estrella más externa, siempre tienen
que aparecer dos _bes_; por lo tanto necesitamos una opción dónde aparezcan solo
aes, este efecto lo podemos lograr si unimos este lenguaje con el lenguaje de
puras _áes_ resultando en la siguiente operación:

{{< katex display >}}(\{a\}^∗\{b\}\{a\}^∗\{b\}\{a\}^∗)^∗\cup\{a\}^* {{< /katex >}} 

{{< /hint >}}


#### Algunas alternativas

Reflexionar por qué las siguientes operaciones también generan el mismo lenguaje.

1. {{< katex >}}\{a\}^∗(\{b\}\{a\}^∗\{b\}\{a\}^∗)^∗ {{< /katex >}} 
1. {{< katex >}}(\{a\}^∗(\{b\}\{a\}^∗\{b\})^*\{a\}^∗ {{< /katex >}}


{{< details title="Respuesta" >}}

1. Las repeticiones provocadas por la cerradura estrella comienzan con _be_,
   como la repetición incluye opcionalmente _aes_ al final, a partir del primer
   par, podrán aparecer _aes_ entre las segundas _bes_ y la siguiente _be_; pero esto
   deja sin la opción de que aparezcan _aes_ al comienzo de la cadena  por lo
   que se concatena estas _aes_ de forma opcional antes de la repetición.
1. En este patrón, la repetición que comience con _aes_ y que estas aparezcan
   entre las segundas _bes_ y las siguiente _be_, sin embargo la repetición no
   garantiza que las cadenas puedan acabar con _aes_ por lo que se concatenan de
   forma opcional al final de esta y no como parte de la repetición.

{{< /details >}}

