---
weight: 2
title: "De lenguajes y palabras"
bookCollapseSection: true
---

{{< section >}}


## Enlaces para explorar

* <i>[Teoría de CONJUNTOS con
    CHOCOLATES](https://www.youtube.com/watch?v=HPlF_fmzN34)</i>. (2021, mar 2). [Video]</br>
* <i>[Cantor's Infinity Paradox | Set Theory](https://www.youtube.com/watch?v=X56zst79Xjg
)</i>. (2018, oct 26). [Video]</br>
* <i>[The Importance of Set Theory | Silvia
    Jonas](https://www.youtube.com/watch?v=9n3QsegLU3U)</i>. (2019, ago 8). [Video]</br>
* <i>[Infinity is bigger than you think -
    Numberphile](https://www.youtube.com/watch?v=elvOZm0d4H0)</i>. (2012, jul 6). [Video]</br>
* <i>[Mathematician Explains Infinity in 5 Levels of Difficulty |
    WIRED](https://www.youtube.com/watch?v=Vp570S6Plt8)</i>. (2023, ene 31). [Video]</br>
* <i>[Introduction to Languages, Strings, and
    Operations](https://www.youtube.com/watch?v=miOofcAiINM)</i>. (2020, apr 22). [Video]</br>
* <i>[Concatenación](https://en.wikipedia.org/wiki/Concatenation)</i>. (s/f). [Wikipedia]</br>
* <i>[Kleene star](https://www.planetmath.org/kleenestar)</i>. (s/f).</br>
* <i>[Clausura de Kleene](https://es.wikipedia.org/wiki/Clausura_de_Kleene)</i>. (s/f). [Wikipedia]</br>

## Lecturas recomendadas

* Kleene, S. C. (1951). Representation of Events in Nerve Nets and Finite
    Automata. RAND PROJECT AIR FORCE SANTA MONICA CA. **Capítulo 1**. 
    [[Liga](https://apps.dtic.mil/sti/citations/ADA596138)]
    [[PDF](https://apps.dtic.mil/sti/pdfs/ADA596138.pdf)]
* Hopcroft, J. E., Motwani, R., Ullman, J. D. Introducción a la teoría de
    autómatas, lenguajes y computación 2a. edición Madrid Pearson Education, 2002. **Sección 2.1**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976666&lang=es&site=eds-live)]
* Carrasco Jiménez, R. C.; Rubio, J. C.; Zubizarreta, M. L. F. Capítulo 1:
    Lenguajes y computadores. Teoría de Lenguajes, Gramáticas y Autómatas Para
    Informáticos, [s. l.], p. 11–22, 2000. **Secciones 2.1 y 2.2**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=ufh&jid=87PC&lang=es&site=eds-live)]
    [[Liga](https://www.digitaliapublishing.com/a/812/teoria-de-lenguajes--gramaticas-y-automatas-para-informaticos)]
* Giró J, Vazquez J, Meloni B, Constable L. Lenguajes Formales y Teoría de
    Autómatas. Alfaomega.     [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001966916&lang=es&site=eds-live)]
    [[Liga](https://libroweb.alfaomega.com.mx/book/lenguajes_formales_teoria_automatas)]
* Hernández Rodríguez LA, Jaramillo Valbuena S, Cardona Torres SA. Practique
    La Teoría de Autómatas y Lenguajes Formales. Ediciones Elizcom; 2010. **Capítulo 1 y 2**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001974225&lang=es&site=eds-live)]
    [[Liga](https://www.proquest.com/docview/2135020663/D0AE11F098D846F9PQ/1)]
* Cantú Treviño, T. G., & Mendoza García, M. G. (2015). Teoría de autómatas :
    un enfoque práctico (Primera edición). Pearson Educación de México.
    **Capítulo 1**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976665&lang=es&site=eds-live)]
* Holt, D. F., Rees, S., & Röver, C. E. (2017). Groups, languages and
    automata. Cambridge University Press. **Capítulo 1**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001959658&lang=es&site=eds-live)]
    [[Liga](https://www.cambridge.org/core/books/groups-languages-and-automata/D31C153F123E5364D1690F697A0F16F2)]
* Brena, R. (2013). Autómatas y lenguajes. McGraw-Hill Interamericana.
    **Capítulo 1**
  [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001698825&lang=es&site=eds-live)]
* Rigo, M. (2014). Formal languages, automata and numeration systems. ISTE.
    **Sección 1.2** 
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001780256&lang=es&site=eds-live)]
    [[Liga](https://onlinelibrary.wiley.com/doi/book/10.1002/9781119008200)]
* Crespi Reghizzi S, Breveglieri L, Morzenti A. Formal Languages and
    Compilation. Second edition. Springer; 2013. **Sección 2.2**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001645742&lang=es&site=eds-live)]
    [[Liga](https://www.springer.com/gp/book/9783030048785)]


