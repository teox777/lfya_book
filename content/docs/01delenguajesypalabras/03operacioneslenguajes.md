---
weight: 23
title: "Operaciones con lenguajes"
description: "Algunas operaciones con conjuntos"
---


### Concatenación de dos lenguajes

La concatenación de dos lenguajes da como resultado un nuevo lenguaje de todas 
las cadenas posibles de cadenas del primer lenguaje concatenadas con el segundo 
lenguaje.

{{< katex >}} L_1L_2 = \{ w_1w_2 | w_1 \in L_1,  w_2 \in L_2 \}{{< /katex >}}


Si suponemos {{< katex >}}L_1=\{aa\}{{< /katex >}},
{{< katex >}}L_2=\{a,b\}{{< /katex >}},
{{< katex >}}L_3=\{\varepsilon\}{{< /katex >}} y
{{< katex >}}L_4=\{a,aa,aaa,aaaa,...\}{{< /katex >}}

1. {{< katex >}}L_1L_2=\{aaa,aab\}{{< /katex >}}
1. {{< katex >}}L_2L_1=\{aaa,baa\}{{< /katex >}}
1. {{< katex >}}L_2L_2=\{aa,ab,ba,bb\}{{< /katex >}}
1. {{< katex >}}L_2L_3=\{a,b\}{{< /katex >}}
1. {{< katex >}}L_1L_4=\{aaa,aaaa,aaaaa,aaaaaa,\ldots\}{{< /katex >}}
1. {{< katex >}}L_4L_3=??{{< /katex >}}

{{< details title="Respuesta ...">}}
{{< katex >}} \{a,aa,aaa,aaaa,\ldots\}{{< /katex >}}
{{< /details >}}


#### Potencia de un lenguaje

La potencia {{< katex >}}n{{< /katex >}} de un lenguaje ({{< katex >}}L^n{{< 
/katex >}}) resulta en un lenguaje conformado por las cadenas que se puedan 
componer concatenando {{< katex >}}n{{< /katex >}} veces cadenas del lenguaje 
{{< katex >}}L{{< /katex >}} original.

##### Ejemplos de potencias de un lenguaje

Suponiendo {{< katex >}}L=\{0,11\}{{< /katex >}}

1. {{< katex >}}L^1 = \{0,11\}{{< /katex >}}
1. {{< katex >}}L^2 = \{00,011,110,1111\}{{< /katex >}}

Considere el siguiente caso:

1. {{< katex >}}L^0 = ?? {{< /katex >}}

¿El lenguaje que se crea si considero no concatenar ninguna de las cadenas del
lenguaje _L_?

{{< details title="Respuesta ...">}}
{{< katex >}} \{ \varepsilon \}{{< /katex >}}
{{< /details >}}


#### Cerraduras de lenguajes

De manera análoga como se creo la potencia estrella de un alfabeto, podemos 
generar la potencia estrella de un lenguaje {{< katex >}}L^*{{< /katex >}} de 
hecho le llamaremos cerradura y tenemos dos opciones:


* Cerradura de Kleene estrella
{{< katex >}}L^*\ = \bigcup_{i=0}^\infty L^i{{< /katex >}}


* Cerradura de Kleene más
{{< katex >}}L^+ = \bigcup_{i=1}^\infty L^i{{< /katex >}}

Notar que la diferencia es que cerrdura _*_ comienza del índice cero y _+_ del
índice _1_. Esta diferencia sutil garantiza que cerradura _*_ siempre incluye a
la cadena vacía _ε_ ya que esta cerradura incluye a la potencia cero de un
lenguaje. 

##### Ejemplos de cerraduras 

Suponiendo {{< katex >}}L=\{aa\}{{< /katex >}}

1. {{< katex >}}L^* = \{\varepsilon,aa,aaaa,aaaaaa,aaaaaaaa,\ldots\}{{< /katex >}}
1. {{< katex >}}L^+ = \{\ \ \ aa,aaaa,aaaaaa,aaaaaaaa,\ldots\}{{< /katex >}}


##### Cerraduras notables

Algunas cerraduras que son relevantes conocer:

1. {{< katex >}}\{\varepsilon\}^* = \{\varepsilon\}{{< /katex >}}
1. {{< katex >}}\{\varepsilon\}^+ = \{\varepsilon\}{{< /katex >}}
1. {{< katex >}}\emptyset^* = \{\varepsilon\}{{< /katex >}}
1. {{< katex >}}\emptyset^+ = \{\ \}{{< /katex >}}


##### La importancia del lenguaje de la cadena vacía

1. {{< katex >}}\{\varepsilon\}L = L{{< /katex >}}
1. {{< katex >}}L\{\varepsilon\} = L{{< /katex >}}
1. {{< katex >}}\{\epsilon,...\}L=(\{\epsilon\}\cup L_r)L=(\{\epsilon\}L)\cup(L_rL)=L\cup 
L_rL{{< /katex >}}

### Operaciones sobre conjuntos

Como los lenguajes son conjuntos, podemos usar las operaciones sobre los 
conjuntos

1. Union: {{< katex >}}L_1 \cup L_2{{< /katex >}}
1. Intersección: {{< katex >}}L_2 \cap L_2{{< /katex >}}
1. Diferencia: {{< katex >}}L_1 - L_2{{< /katex >}}
1. Complemento: {{< katex >}}\overline{L} = \Sigma^* - L{{< /katex >}}


### Visualización de operaciones con lenguajes

#### Unión

<center>
{{< figure src="../unión.png" title="Unión entre dos lenguajes" >}}
</center>

El resultado de la unión entre dos lenguajes es un nuevo conjunto (<span
style="color:#F6921D;">⬤ naranja fuerte</span>) que contiene todas las cadenas del 
lenguaje uno (<span style="color:#D56161;">⬤ rojo</span>) y lenguaje dos
(<span style="color:#F6B26B;">⬤ naranja suave</span>). 

#### Concatenación

<center>
{{< figure src="../concatenación.png" title="Concatenación entre dos lenguajes" >}}
</center>


El resultado de la concatenación entre dos lenguajes es un nuevo conjunto (<span
style="color:#F6921D;">⬤ naranja fuerte</span>) que contiene todas las cadenas
que se pueden formar de concatenar cadenas del lenguaje uno (<span
style="color:#D56161;">⬤ rojo</span>) con cadenas del lenguaje dos
(<span style="color:#F6B26B;">⬤ naranja suave</span>). 

Sin embargo es importante notar, que si uno de los lenguajes contiene a la
cadena vacía (𝜺) entonces, el lenguaje contrario está contenido en el resultado.

<center>
{{< figure src="../concatenación2.png" title="Concatenación entre dos lenguajes, el lenguaje que queda afuera contiene a la cadena vacía (𝜺)" >}}
</center>

Por otro lado, si ambos lenguajes contienen a la cadena vacía (𝜺) ambos son
parte del resultado.

<center>
{{< figure src="../concatenación3.png" title="Concatenación entre dos lenguajes, ambos contienen a la cadena vacía (𝜺)" >}}
</center>


#### Cerradura

<center>
{{< figure src="../cerradura.png" title="Concatenación entre dos lenguajes" >}}
</center>

El resultado de la cerradura de un lenguaje es un nuevo lenguaje (<span
style="color:#F6921D;">⬤ naranja fuerte</span>) que contiene todas las cadenas
que se pueden formar de concatenar de cero a más veces las cadenas de lenguaje
original  (<span style="color:#D56161;">⬤ rojo</span>). Como una de las
concatenaciones que se permite es la de una vez de las cadenas del lenguaje,
entonces el resultado siempre contiene al lenguaje original.

#### Complemento

<center>
{{< figure src="../complemento.png" title="Concatenación entre dos lenguajes" >}}
</center>

El resultado del complemento de un lenguaje es un lenguaje (<span
style="color:#F6921D;">⬤ naranja fuerte</span>) que contiene todas las cadenas
que no están en el lenguaje original  (<span style="color:#D56161;">⬤ rojo</span>).

