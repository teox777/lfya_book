---
weight: 21
title: "Un modelo de computadora"
description: "Presentamos el modelo de máquina computacional que usaremos"
---

Recordemos que queremos responder a la pregunta de _¿Cuáles son las propiedades 
escenciales de una computadora?_ Para lograr esto debemos definir una forma de 
hablar de cualquier máquina capaz de hacer cálculos o computar. Sin embargo hay
muchas formas que puede tomar una máquina que computa _¿cómo vamos a poder generalizar sobre todas estas 
máquinas?_ Necesitamos un modelo general de máquina computacional. Para lograr 
esto vamos a concentrarnos en su funcionalidad y no tanto en sus partes, por lo
que podemos pensar que es una [caja
negra](https://es.wikipedia.org/wiki/Caja_negra_(psicolog%C3%ADa)).

Podemos comenzar con que una máquina que computa es un <mark>objeto
físico 💻</mark> que 
interactúa con su ambiente: <mark>el mundo 🌎</mark>. Para lograr esa interacción necesita 
datos/información de entrada que internamente serán procesados para producir 
datos/información de salida. Esta es una generalización habitual para nosotros, 
sin embargo ¿de cuántas entradas estamos hablando? 🤔 las computadoras que 
habitualmente usamos tiene muchas: teclado, ratón, pantalla táctil, antena 
internet, joystick, etc.; igualmente para las salidas tenemos muchas opciones, 
como la pantalla, la impresora, las bocinas, la misma antena de internet, etc.
La representación gráfica de esta conceptualización sería la siguiente: 

<center>
{{< figure src="../cajanegra.svg" title="Caja negra con múltiples entradas y múltiples salidas" >}}
</center>

Para simplificar este modelo vamos asumir que nuestra máquina de cómputo recibe una sola entrada 
en forma secuencial, es decir todas las entradas habituales de nuestra 
computadora las podemos poner una tras otra de forma serial. Existe otro
problema con los tipos de entradas, muchas veces nuestros datos
tienen diferente _'forma'_ como números, cadenas, booleanos. Vamos a volver 
asumir una posición simplista y nada más vamos a dejar que nuestra única 
entrada reciba únicamente cadenas, los números y booleanos los podríamos 
codificar como cadenas y de esa forma incluirlos. A lo mejor se preguntan cómo 
podemos poner todas las entradas una tras otra, sin embargo podemos sincronizar 
las entradas o podemos usar marcas conformadas por cadenas que identifique de 
qué dispositivo viene la entrada. Por ejemplo:

<center>
<em>MOUSE:200,300,click;KEYBOARD:k;INTERNET:{name:ivan}...</em> 
</center>

Con respecto a la salida podríamos asumir la misma convención y determinar que 
sólo hay una salida y es de tipo cadena. Sin embargo, vamos a ir un poco más 
allá y vamos a poner de lado la mayor parte de la información generada por el 
proceso, y nos vamos a concentrar en si la máquina de cómputo dada la entrada 
que le dimos pudo hacer algo _'útil'_ con ella o no.  Esto deja del lado 
muchos aspectos: como si el resultado se obtuvo de forma 
rápido, si se ve bonito o si el programa que lo genera fue "bien" hecho _¿Esto quiere decir que la salida no es importante para una máquina de cómputo?_ __No__, quiere decir que para lo que 
resta de esta abstracción de máquina computacional el resultado lo vamos a 
considerar como un efecto secundario de ejecutar el proceso codificado en la 
máquina. Lo importante es si la entrada sirvió para generar el resultado 
o no. Al resultado "correcto" lo marcaremos como "verdadero" y 
al incorrecto como "falso".


### Una abstracción de máquina computacional

En el resto del material asumiremos que una máquina computacional es aquella 
que podemos abstraer como una **caja negra** que sólo tiene una entrada de tipo 
cadena y sólo tiene una salida que toma la forma de _verdadero_ (true) o 
_falsa_ (false). 

<center>
{{< figure src="../cajanegra2.svg" title="Una entrada y una salida" >}}
</center>


### Experimentando con nuestra abstracción

Ya que tenemos nuestra abstracción de tipo caja negra podemos imaginarnos que 
podemos experimentar con ella. Podemos tomar una entrada y podemos ver si puede 
hacer algo 'útil' o no. Esto nos va a permitir ligar el comportamiento de la 
**caja negra** con el exterior, de hecho algo que podríamos hacer es pasar 
todas las entradas posibles y ver su comportamiento para todas las entradas. Por supuesto, esta línea de pensamiento tiene el problema que existen un 
infinito número de entradas posibles entonces nunca acabaríamos el análisis. Sin embargo, a 
lo mejor existe la posibilidad de establecer una liga entre el funcionamiento de
una máquina computacional con la bastedad de posibilidades de entradas. Pronto veremos que esto 
es posible y el infinito no es un límite de las máquinas computacionales.

### Ventajas de nuestra abstracción

Nuestra abstracción tienen las siguientes ventajas:

* Aunque representa un objeto físico, no depende de elementos físicos 
  tecnología. La caja podría ser:
  [eléctrica](https://es.wikipedia.org/wiki/Computadora_anal%C3%B3gica),
  [electrónica](https://es.wikipedia.org/wiki/Computadora),
  [óptica](https://es.wikipedia.org/wiki/Computadora_%C3%B3ptica),
  [cuántica](https://es.wikipedia.org/wiki/Computaci%C3%B3n_cu%C3%A1ntica) o
  [mecánica](https://cienciaes.com/biografias/2010/06/26/la-computadora-mecanica-de-charles-babbage/).
* No dependemos del tipo de la información, todo se pone en forma secuencial y 
  de salida sólo se codifica si la máquina pudo hacer algo 'útil' o no con dicha
  entrada.
* El comportamiento de la máquina está configurado internamente dentro de esta.


