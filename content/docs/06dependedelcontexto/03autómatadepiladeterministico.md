---
weight: 63
title: "Automata de Pila determinístico"
description: "Se presentan las condiciones necesarias ara que un AP sea un
Autómata de Pila Determinístico"
---

En [sección anterior]({{< ref "02lenguajesdepalíndromos#lenguaje-de-palíndromo" >}}) vimos diferentes
versiones de lenguajes de palíndromos, y aunque sus gramáticas fueron no
ambiguas tres de estos lenguajes resultaron en un Autómata de Pila no
determinístico; en este caso el no determinismo del autómata no solo hace que el
autómata esté en varios estados, pero estos están asociados a diferentes
configuraciones de la pila; sin lugar a dudas el no determinismo complica más el
análisis de una cadena.

Desafortunadamente para los lenguajes con formas: _wwʳ_, 
_wawʳ_ y _wwʳ_ no es posible construir un AP no determinismo. Esto es derivado
de la naturaleza del lenguaje, ya que dada la información que sabe el AP,
estado, símbolo actual en la cadena y símbolo de la pila, no es posible saber
que estrategia seguir: si seguir acumulando símbolos en la pila o ya llego a la
mitad y tiene que comenzar a cancelarlos. Por supuesto, para nosotros con una
cadena pequeña es fácil definir la estrategia, identificamos la mitad de la
cadena y durante la primera mitad agregamos símbolos a la pila y durante la
segunda quitamos símbolos. Sin embargo, el AP no tiene el lujo de ver toda la
cadena sino símbolo por símbolo. Esto es diferente para el lenguaje con cadenas
de la forma _wmwʳ_, ya que la marca _m_ nos especifica la mitad de la cadena,
entonces no es necesario adivinar dónde está la mitad, como en los otros casos,
donde como resultado que este lenguaje sí tiene un AP determinístico.

### APD

<div class="definition">
Un <mark>Autómata de Pila Determinístico (APD)</mark> es una tupla {{< katex
>}}(Q,\Sigma,\Gamma,q_0,Z_0,A, \delta ){{< /katex >}} donde:

* {{< katex >}}Q{{< /katex >}} es un conjunto de estados finitos
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de símbolos terminales
* {{< katex >}}\Gamma{{< /katex >}} es un alfabeto de la pila
* {{< katex >}}q_0{{< /katex >}} es un estado que denominaremos inicial donde
    {{< katex >}}q_0 \in Q{{< /katex >}}
* {{< katex >}}Z_0{{< /katex >}} es un símbolo de la pila que denominaremos
    inicial de la pila donde
    {{< katex >}}Z_0 \in \Gamma{{< /katex >}}
* {{< katex >}}A{{< /katex >}} es un conjunto de estados que denominaremos
    finales donde {{< katex >}}A \subset Q{{< /katex >}}
* {{< katex >}}\delta{{< /katex >}} es una función de transición que cumple con:
    {{< katex >}}\delta:Q \times (\Sigma\cup \{\varepsilon\}) \times \Gamma \rightarrow Q \times \Gamma^*{{< /katex >}}
</div>

y además cumple con las siguientes restricciones:

* Solo hay una transición {{< katex >}}\delta(q,a,x){{< /katex >}} por estado
    donde {{< katex >}}q \in Q, a \in \Sigma, x \in \Gamma{{< /katex >}}
* Y si existe {{< katex >}}\delta(q,\varepsilon,x){{< /katex >}} no existe
    ninguna transición con la forma  {{< katex >}}\delta(q,a,x){{< /katex >}}
    donde {{< katex >}}q \in Q, a \in \Sigma, x \in \Gamma{{< /katex >}}

Esta definición es seguida por la definición [AP las cadenas con la forma _wmwʳ_]({{<
ref "02lenguajesdepalíndromos#ap-para-wmwʳ" >}}). Para los otros AP vistos en
esa sección  no se cumplen estas restricciones:

* [_wwʳ_]({{< ref "02lenguajesdepalíndromos#ap-para-wwʳ" >}}) para el estado
    inicial rompen la segunda restricción en relación a épsilon, ya que existen
    transiciones por épsilon y símbolo usando usando el mismo símbolo de la
    pila.
* [_wawʳ_]({{< ref "02lenguajesdepalíndromos#ap-para-wawʳ" >}}) para el estado
    inicial rompen la primer restricción en relación a los símbolo, ya que
    existen más de una transición con el mismo símbolo del alfabeto y símbolo
    de la pila.
* [_w=wʳ_]({{< ref "02lenguajesdepalíndromos#ap-para-w=wʳ" >}}) para el estado
    inicial rompen la primer y segunda restricción. 

## Propiedad de determinismo asociado al lenguaje

Un lenguaje será determinístico si es posible construir un APD que lo acepte,
sino es posible será no determinístico. De esta forma la propiedad de
determinismo está asociada al lenguaje y no al AP.

### Relación entre ambigüedad y no determinismo

En los ejemplos visto para los lenguajes de palíndromos vimos que no hay relación
entre ambigüedad y no determinismo; un cuando los lenguajes son no ambiguos los
lenguajes fueron no determinístico o determinístico. Sin embargo, si un lenguaje
ambiguo será no determinístico, no habrá forma de procesar las cadenas sin
explotar la ambigüedad; por otro lado, si el lenguaje es determinístico quiere
decir que existe una gramática no ambigua que lo genera, y por lo tanto el
lenguaje es no ambiguo. En resumen estos son los casos posibles:

* Lenguaje no ambiguo, lenguaje determinista o no determinista
* Lenguaje ambiguo, lenguaje no determinista
* Lenguaje determinista, lenguaje no ambiguo
* Lenguaje no determinista, lenguaje ambiguo o no ambiguo



