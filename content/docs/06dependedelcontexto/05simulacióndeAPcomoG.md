---
weight: 65
title: "Simulación de AP como G"
description: "Se explica que es posible simular una AP como un G"
---

Para simular un AP con una gramática es necesario generar transformar las
transiciones con forma {{< katex >}}a,A/\alpha{{< /katex >}} en la siguiente
forma {{< katex >}}A \rightarrow a\alpha {{< /katex >}}.

### Casos dado el tipo de transición

1. Si la transición es un _push_ la forma de la regla queda:
   * {{< katex >}} Z_0 \rightarrow aAZ_0 {{< /katex >}}
   * {{< katex >}} A \rightarrow aAA {{< /katex >}}
2. Si la transición es un _pop_ la forma de la regla queda:
   * {{< katex >}} A \rightarrow a {{< /katex >}}
3. Si la transición que no modifica la pila la forma de la regla queda:
   * {{< katex >}} Z_0 \rightarrow mZ_0 {{< /katex >}}
   * {{< katex >}} A \rightarrow mA {{< /katex >}}
4. Si es la transición de término del AP la forma de la regla queda:
   * {{< katex >}} Z_0 \rightarrow \varepsilon {{< /katex >}}


####  Ejemplo

<center>
{{< figure src="../wmwr.svg" title="AP palíndromos con marca" >}}
</center>

La gramática que lo simula luce:
 {{< katex display >}}
\begin{array}{ll} 
Z_0\rightarrow & aAZ_0\\
Z_0\rightarrow &bBZ_0\\
A\rightarrow &aAA\\
B\rightarrow &aAB\\
A\rightarrow &bBA\\
B\rightarrow &bBB\\
Z_0\rightarrow &mZ_0\\
A\rightarrow &mA\\
B\rightarrow &mB\\
A\rightarrow &a\\
B\rightarrow &b\\
Z_0\rightarrow &\epsilon
 \end{array}
 {{< /katex >}}
