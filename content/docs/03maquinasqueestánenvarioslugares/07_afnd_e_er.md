---
weight: 37
title: "ER → AFND-ε"
description: "Se explica el proceso para transformar toda ER a un AFND-ε"
---

### Aplicando operaciones de Lenguajes regulares a AFND-ε

Si tengo dos AFND-ε:

* {{< katex >}}(Q^A,\Sigma,q^A_0,A^A,\delta^A){{< /katex >}} asociado al
    lenguaje {{< katex >}}L^A{{< /katex >}}
* {{< katex >}}(Q^B,\Sigma,q^B_0,A^B,\delta^B){{< /katex >}} asociado al
    lenguaje {{< katex >}}L^B{{< /katex >}}

Entonces es posible construir los siguientes AFND-ε basados en la operaciones de
los Lenguajes Regulares

* {{< katex >}}L^A+L^B{{< /katex >}}</br>
  {{< katex display>}}
  \left( Q^A \bigcup Q^B \bigcup \{q^{union}_0\}),\Sigma,q^{union}_0,A^A \bigcup A^B,
  \delta^A \bigcup \delta^B \bigcup \{(q^{union}_0,\varepsilon)
  \rightarrow \{q^A_0, q^B_0\}\} \right)
  {{< /katex >}}
* {{< katex >}}L^AL^B{{< /katex >}}</br>
  {{< katex display>}}
  \left( Q^A \bigcup Q^B),\Sigma,q^A_0,A^A \bigcup A^B,
  \delta^A \bigcup \delta^B \bigcup \{(q,\varepsilon)
  \rightarrow \{q^B_0\} | q \in A^A\} \right)
  {{< /katex >}}
* {{< katex >}}L^*{{< /katex >}}</br>
  {{< katex display>}}
  \left(Q,\Sigma,q^{cerradura}_0,A\bigcup \{q^{cerradura}_f\},
  \delta \bigcup \{ 
  (q^{cerradura}_0,\varepsilon) \rightarrow \{q_0\},
  (q^{cerradura}_0,\varepsilon) \rightarrow \{q^{cerradura}_f\},
  (q^{cerradura}_f,\varepsilon) \rightarrow \{q^{cerradura}_0\}\}
  \bigcup \{(q,\varepsilon) \rightarrow \{q^{cerradura}_f \} | q \in A\} 
  \right)
  {{< /katex >}}


#### Ejemplo

Supongamos los lenguajes {{< katex >}}L_1: 0^*1 {{< /katex >}} y 
{{< katex >}}L_2: 10^* {{< /katex >}} 

Con sus autómatas gráficos

<center>
{{< figure src="../unos_cero.svg" title="AF par 0*1" >}}
</center>

<center>
{{< figure src="../uno_ceros.svg" title="AF par 10*" >}}
</center>

Si aplicamos cada una de las operaciones obtenemos:

* Unión {{< katex >}}L_1 \bigcup L_2 {{< /katex >}} 
<center>
{{< figure src="../union.svg" title="0*1+10*" >}}
</center>


* Concatenación {{< katex >}}L_1  L_2 {{< /katex >}} 
<center>
{{< figure src="../concat.svg" title="0*110*" >}}
</center>

* Cerradura {{< katex >}}L_1^* {{< /katex >}} 
<center>
{{< figure src="../cerradura.svg" title="(0*1)*" >}}
</center>

### Ejemplo de AFND-ε a ER

Dado a que tenemos una forma de hacer las operaciones de lenguajes regulares con
AFND-ε, podemos pensar en transformar un ER a un AFND, partiendo de AF básicos e
ir aplicando las operaciones


* {{< katex >}}(a^*ba^*ba^*)+a^*{{< /katex >}} 
<center>
{{< figure src="../reg.svg" title="AFND-ε para (a*ba*ba*)+a" >}}
</center>
