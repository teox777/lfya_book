---
weight: 43
title: "ER como gramática no ambigua"
description: "Ejemplo de gramática no ambigua para el lenguaje de las Expresiones Regulares"
---

Como vimos la gramática del lenguaje de las Expresiones Regulares puede ser
ambigua, sin embargo
es posible generar una gramática para el lenguaje que no es ambigua:

 {{< katex display >}} 
 \begin{array}{rl}
E \rightarrow & T | E+T \\
T \rightarrow & F | TF \\
F \rightarrow & B | F^* | (E) \\
B \rightarrow & a | b | \varepsilon | \emptyset \\
 \end{array}
 {{< /katex >}} 

Con esta gramática se obtiene la siguiente derivación:

{{< katex display >}} 
 \begin{array}{rl}
E \Rightarrow &  T\\
  \Rightarrow &  F\\
  \Rightarrow &  F^*\\
  \Rightarrow &  (E)^*\\
  \Rightarrow &  (T)^*\\
  \Rightarrow &  (TF)^*\\
  \Rightarrow &  (TFF)^*\\
  \Rightarrow &  (TFFF)^*\\
  \Rightarrow &  (TFFFF)^*\\
  \Rightarrow &  (FFFFF)^*\\
  \Rightarrow &  (F^*FFFF)^*\\
  \Rightarrow &  (B^*FFFF)^*\\
  \Rightarrow &  (a^*bFFF)^*\\
  \Rightarrow &  (a^*ba^*FF)^*\\
  \Rightarrow &  (a^*ba^*BF)^*\\
  \Rightarrow &  (a^*ba^*bF)^*\\
  \Rightarrow &  (a^*ba^*bF^*)^*\\
  \Rightarrow &  (a^*ba^*bB^*)^*\\
  \Rightarrow &  (a^*ba^*ba^*)^*\\
 \end{array}
{{< /katex >}} 

Esta es la única derivación, y por lo tanto sólo tiene un árbol de derivación:

<center>
{{< figure
src="../arboles_re.png" title="Árbol para la cadena: (a*ba*ba*)" >}}
</center>

De hecho para todas las cadenas sólo habrá un árbol de derivación, es decir esta
gramática es no ambigua. 

Con esto se puede apreciar que un lenguaje puede ser generado por gramática
ambigua o no ambigua.

