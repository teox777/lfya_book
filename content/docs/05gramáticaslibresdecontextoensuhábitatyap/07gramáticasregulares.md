---
weight: 47
title: "Gramáticas Regulares"
description: "Se presentan las gramáticas regulares"
---

En la sección anterior vimos que un AF se puede transformar en una GLC ¿La
dirección contraria se puede? Es decir una GLC se pude transformar en un AF.
Sabemos que no se pude para todos, porque existen GLC que generan lenguajes que
no son regulares ¿Entonces bajo que condición se podría? Sólo se podrá para lo
que denominaremos como:


### GR

<div class="definition">
Una <mark>Gramática Regular (GLC)</mark> es una tupla {{< katex >}}(V,\Sigma,P,S){{< /katex >}} donde:


* {{< katex >}}V{{< /katex >}} es un alfabeto de símbolos no
    terminales o símbolos auxiliares
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de símbolos
    terminales, que conforman nuestras cadenas
* {{< katex >}}P{{< /katex >}} es un conjunto de reglas que con la forma
    <mark>{{< katex >}}A \rightarrow  bB | c {{< /katex >}}</mark> donde 
    {{< katex >}}b \in  \Sigma, c \in  \Sigma, A \in V, B \in V   {{< /katex >}}
* {{< katex >}}S \in V{{< /katex >}} lo denominamos símbolo inicial
</div>

Si comparamos lo único que cambia con respecto a las GLC es la forma de la
regla. Pasa de ser: 
{{< katex display >}}A \rightarrow  \alpha{{< /katex >}} 
A esta forma que es mas restrictiva:
{{< katex display>}}A \rightarrow  bB | c {{< /katex >}}



Con esto en mente, toda GR se puede convertir en un AF siguiendo un proceso
inverso al presentado en la [sección anterior]({{< ref "../05gramáticaslibresdecontextoensuhábitatyap/05leguajesregularesyglc/" >}}).

